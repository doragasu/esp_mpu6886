# Introduction

This is a driver for the InvenSense MPU-6886 motion tracking chip (accelerometer + gyroscope) for ESP8266 and ESP32 devices. It is intended to be used with the `esp-idf` or `ESP8266_RTOS_SDK`, and has no Arduino dependencies. I2C is used for communications. The driver aims to be flexible and simple. Driver testing is currently done using an M5Atom Matrix board, so I know that at least there it is working.

# Usage

As with any `esp-idf` or `ESP_OPEN_RTOS` component, clone this project in the `components` subdirectory of your project. Then you should be able to `#include <mpu6886.h>` and start using the module right away. Using the module is easy: just call `mpu6886_init()` once, and then call `mpu6886_accel_raw_get()` and `mpu6886_gyro_raw_get()` to get acceleration and rotation data respectively. Optionally you have functions to read/write other device registers, so you can set any configuration as required.

This module has a dependency on the [M5Stack Core I2C](https://gitlab.com/doragasu/m5stack_core_i2c), so make sure to also add it as a component or the project will not build.

Currently the driver is a bit bare-bones, but I plan to add support for other things, like more flexible device configuration, changing scales, unit conversion, and interrupts.

# License

This program is provided with NO WARRANTY, under the [Mozilla Public License (MPL)](https://www.mozilla.org/en-US/MPL/2.0/). I am not responsible in any way if the code does not work, or causes any harm. Make sure you read the warning above in this regard.

