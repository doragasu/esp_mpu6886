#include <core_i2c.h>
#include <endian.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include <mpu6886.h>

#define MS_TO_TICKS(ms) ((ms)/portTICK_RATE_MS)
#define vTaskDelayMs(ms) vTaskDelay(MS_TO_TICKS(ms))

static struct ci2c_intf mpu6886 = {
	.dev_addr = MPU6886_ADDR
};

esp_err_t mpu6886_read(enum mpu6886_reg reg, uint8_t *data)
{
	return ci2c_read(reg, data, 1, &mpu6886);
}

esp_err_t mpu6886_word_read(enum mpu6886_reg reg, uint16_t *data)
{
	uint16_t scratch;

	esp_err_t err = ci2c_read(reg, (uint8_t*)&scratch, sizeof(scratch), &mpu6886);
	if (!err) {
		*data = be16toh(scratch);
	}

	return err;
}

esp_err_t mpu6886_write(enum mpu6886_reg reg, uint_fast8_t data)
{
	uint8_t value = data;
	return ci2c_write(reg, &value, 1, &mpu6886);
}

esp_err_t mpu6886_word_write(enum mpu6886_reg reg, uint_fast16_t data)
{
	const uint16_t scratch = htobe16(data);
	return ci2c_write(reg, (const uint8_t*)&scratch, sizeof(scratch), &mpu6886);
}

esp_err_t mpu6886_bit_set(enum mpu6886_reg reg, uint_fast8_t bit)
{
	uint8_t data;
	esp_err_t err = mpu6886_read(reg, &data);

	if (ESP_OK == err) {
		err = mpu6886_write(reg, data | BIT(bit));
	}

	return err;
}

esp_err_t mpu6886_bit_clear(enum mpu6886_reg reg, uint_fast8_t bit)
{
	uint8_t data;
	esp_err_t err = mpu6886_read(reg, &data);

	if (ESP_OK == err) {
		err = mpu6886_write(reg, data & ~BIT(bit));
	}

	return err;
}

static esp_err_t coord_3d_get(enum mpu6886_reg reg, int16_t *x,
		int16_t *y, int16_t *z)
{
	esp_err_t err;
	uint16_t data[3];

	err = ci2c_read(reg, (uint8_t*)data, sizeof(data), &mpu6886);
	if (!err ) {
		*x = be16toh(data[0]);
		*y = be16toh(data[1]);
		*z = be16toh(data[2]);
	}

	return err;
}

esp_err_t mpu6886_accel_raw_get(int16_t *x, int16_t *y, int16_t *z)
{
	return coord_3d_get(MPU6886_REG_ACCEL_XOUT_H, x, y, z);
}

esp_err_t mpu6886_gyro_raw_get(int16_t *x, int16_t *y, int16_t *z)
{
	return coord_3d_get(MPU6886_REG_GYRO_XOUT_H, x, y, z);
}

void mpu6886_init(void)
{
	uint8_t id = 0;

	ESP_ERROR_CHECK(mpu6886_read(MPU6886_REG_WHO_AM_I, &id));
	if (0x19 != id) {
		abort();
	}

	// Reset chip and take it out from sleep
	ESP_ERROR_CHECK(mpu6886_write(MPU6886_REG_PWR_MGMT_1, 0x80));
	vTaskDelayMs(20);
	ESP_ERROR_CHECK(mpu6886_write(MPU6886_REG_PWR_MGMT_1, 0x01));
	vTaskDelayMs(20);
}
